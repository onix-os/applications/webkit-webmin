import webview

from webview.platforms.cef import settings
settings.update({
    'persist_session_cookies': True
})

webview.create_window('Webmin', 'https://localhost:10000')
webview.start(gui='cef')